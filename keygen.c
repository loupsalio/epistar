#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <time.h>

#define BUFFER 8

void		keygen()
{
	FILE	*rd;
  	char 	str[BUFFER + 1] = {0};
   	char 	*hex= "0123456789ABCDEF";
   	int		i;

   srand(time(NULL));
   for(i = 0 ; i < BUFFER; i++)
   {
      str[i] = hex[(rand() % 16)];
   }
	rd = fopen("Keygen.txt", "w");
	if(rd == NULL)
    	exit(-1);
	fprintf(rd, "%s", str);
	fclose(rd);
}

int		main(void){
	keygen();
	return(0);
}