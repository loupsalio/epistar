$(document).ready(function(){

    $('input[type="checkbox"][name="onoffswitch"]').change(function() {
        if(this.checked) {
            $.ajax({
                type: 'POST',
                url: "/set_on",
                error: function (request, error) {
                    console.log("Erreur : responseText: " + request.responseText);
                },
                success: function (data) {
                    console.log("on")
                }
            });

        }
        else
        {
            $.ajax({
                type: 'POST',
                url: "/set_off",
                error: function (request, error) {
                    console.log("Erreur : responseText: " + request.responseText);
                },
                success: function (data) {
                    console.log("off")
                }
            });
        }
    });

    $('#main').on('click', ".delete_right", function () {
        var name = $(this).parent().parent().parents().children('.name_of_repo').text();
        var user = $(this).parent().children('.name_user').text();
        var right = $('#right_' + name + ' option:selected').text();
        var my_core = $(this).parent().parent().parent();
        if(!confirm("Delete " + user + " rights for " + name + " repo ?"))
            return;
        $(this).parent().parent().parent().html("Wait please...");
        $.ajax({
            type: 'POST',
            url: "/del_rights",
            data: "name=" + name + "&user=" + user,
            error: function (request, error) {
                alert("Erreur : responseText: " + request.responseText);
            },
            success: function (data) {
                my_core.html(data);
            }
        });
    });

    $('#main').on('click', ".send_new_right", function () {
        var name = $(this).parent().parent().parents().children('.name_of_repo').text();
        var user = $(this).parent().children('.add_user_right_name').val();
        var right =  $(this).parent().children('.add_user_right').val();
        var my_core = $(this).parent().parent();
        if(!confirm("Add " + right + " rights to " + user + " for " + name + " repo ?"))
            return;
        $(this).parent().parent().html("Wait please...");
        $.ajax({
            type: 'POST',
            url: "/change_rights",
            data: "name=" + name + "&user=" + user + "&rights=" + right,
            error: function (request, error) {
                alert("Erreur : responseText: " + request.responseText);
            },
            success: function (data) {
                my_core.html(data);
            }
        });
    });

    $('#main').on('click', ".save_right", function () {
        var name = $(this).parent().parent().parents().children('.name_of_repo').text();
        var user = $(this).parent().children('.name_user').text();
        var right = $('#right_' + name + ' option:selected').text();
        var my_core = $(this).parent().parent().parent();
        if(!confirm("Change " + user + " rights for " + name + " repo ?"))
            return;
        $(this).parent().parent().parent().html("Wait please...");
        console.log(name);
        $.ajax({
            type: 'POST',
            url: "/change_rights",
            data: "name=" + name + "&user=" + user + "&rights=" + right,
            error: function (request, error) {
                alert("Erreur : responseText: " + request.responseText);
            },
            success: function (data) {
                my_core.html(data);
            }
        });
    });

    $('#main').on('click', ".key-delete-button", function () {
        var name = $(this).parent().parent().children('li').text();
        if(!confirm("Delete " + name + " key ?"))
            return;
        $(this).parent().parent().parent().html("Wait please...");
        $.ajax({
            type: 'POST',
            url: "/del_key",
            data: "name=" + name,
            error: function (request, error) {
                alert("Erreur : responseText: " + request.responseText);
            },
            success: function (data) {
                $("#list_of_keys").html(data);
            }
        });

    });

    $('#main').on('click', ".delete_button_user", function () {
        var login = $(this).parent().parent().children('.user_login').text();

        if(!confirm("Delete " + login + " user ?"))
            return;
        console.log(login);
              $.ajax({
                type: 'POST',
                url: "/del_user",
                data: "login=" + login,
                error: function (request, error) {
                    alert("Erreur : responseText: " + request.responseText);
                },
                success: function (data) {
                    $('#list_users').html(data);
                }
            });

    });

    $('#main').on('click', "#add_user", function () {

        var login = $('#login').val();
        var passwd = $('#passwd').val();
        var mail = $('#mail').val();
        if (login != null && mail != null) {
            $.ajax({
                type: 'POST',
                url: "/add_user",
                data: "login=" + login + "&passwd=" + passwd + "&mail=" + mail,
                error: function (request, error) {
                    alert("Erreur : responseText: " + request.responseText);
                },
                success: function (data) {
                   $('#list_users').html(data);
                    $('#login').val('');
                    $('#passwd').val('');
                    $('#mail').val('');
                }
            });
        }
    })

    $('#main').on('click', ".delete-button", function () {
        var name = $(this).parent().parent().children('li').text();
        if(!confirm("Delete " + name + " repo ?"))
            return;
        $(this).parent().parent().parent().html("Wait please...");
        console.log(name);
        $.ajax({
            type: 'POST',
            url: "/del",
            data: "name=" + name,
            error: function (request, error) {
                alert("Erreur : responseText: " + request.responseText);
            },
            success: function (data) {
               $("#list_of_repos").html(data);
            }
        });

    });

    $('#main').on('click', ".key-div",function () {
        $(this).siblings().children(".key-options").hide(500);
        $(this).children(".key-options").toggle(500);
    })

    $('#main').on('click', ".name_of_repo", function(){
        $(this).css("color", "green");
        var elem = $(this);
        $(this).parents().siblings().children(".repo-options").hide(500);
        var tmp = $(this).parents();
        console.log(tmp.children('.name_of_repo').text());
        $.ajax({
            type: 'POST',
            url      : "/acl",
            data     : {"name": tmp.children('.name_of_repo').text()},
            error    : function(request, error) {
                alert("Erreur : responseText: "+request.responseText);
            },
            success  : function(data) {
                elem.css("color", "black");
                tmp.children(".repo-options").children(".rights").html(data);
                tmp.children(".repo-options").show(500);
            }
        });
    });
});
