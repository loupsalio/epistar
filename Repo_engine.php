<?php
/**
 * Created by PhpStorm.
 * User: Windows
 * Date: 17/02/2018
 * Time: 22:13
 */

namespace epistar;

require "ssh_key.php";

class Repo_engine
{
    private $auth;

    public function __construct()
    {
        if (!empty($_SESSION["auth"])){
            $this->auth = $_SESSION["auth"];
        }
    }

    public function get_repos_list(){
        $cmd = "python3 blih.py -u " . $this->auth->get_mail() . " -t \"" . $this->auth->get_password() . "\" repository list";
        $str = shell_exec($cmd);
        $t = strpos($str, "Error");
        if($t != false) {
            return null;
        }
            $res = explode("\n", $str);
        asort($res);
        array_shift($res);
        if (empty($res))
            $res = "No repos";
        return $res;
    }

    public function get_keys_list(){
        $list = array();

        $cmd = "python3 blih.py -u " . $this->auth->get_mail() . " -t \"" . $this->auth->get_password() . "\" sshkey list";
        $str = shell_exec($cmd);
        $t = strpos($str, "Error");
        if($t != false) {
            return null;
        }
        $res = explode("\n", $str);
        array_pop($res);
        foreach ($res as $t){
            $elem = explode(" ", $t);
            array_push($list, new ssh_key($elem[2], $elem[1]));
        }
        asort($list);
        if (empty($list))
            $res = "No repos";
        return $list;
    }

    public function list_rights($arg){
        $list_right = array();
        array_push($list_right, "r");
        array_push($list_right, "w");
        array_push($list_right, "x");
        array_push($list_right, "rw");
        array_push($list_right, "rx");
        array_push($list_right, "wx");
        array_push($list_right, "rwx");

        foreach ($list_right as $f){
            if ($f == $arg){
                echo '<OPTION selected>'.$f;
            }
            else
                echo '<OPTION>'.$f;
        }

    }

    public function get_acl($name){
        $str = shell_exec("python3 blih.py -u " . $this->auth->get_mail() . " -t \"" . $this->auth->get_password() . "\" repository getacl " . $name);
        $t = strpos($str, "Error");
        if($t != false) {
            echo "No acl";
            echo '<div><span>Add rights </span><input type="text" class="add_user_right_name"/>';
            echo '<SELECT class="add_user_right">
        <option>r</option>
        <option>w</option>
        <option>x</option>
        <option>rw</option>
        <option>rx</option>
        <option>wx</option>
        <option>rwx</option>        
        </SELECT>';
            echo '<button class="send_new_right">Add right</button></div>';
            return;
        }
        $res = explode("\n", $str);
        asort($res);
        array_shift($res);
        foreach ($res as $t){
            $tmp = explode(":", $t);
            echo "<div class='right_block'>";
            echo "<p><span class='name_user'>$tmp[0]</span>&nbsp;";

            echo '<SELECT name="right" style="width: 60px; color: white;" id="right_'.$name.'" size="1">';
            $this->list_rights($tmp[1]);
            echo '</SELECT>';
            echo '&nbsp;<i class="fas fa-save save_right"/>';
            echo '&nbsp;<i class="fas fa-trash delete_right" id="delete_rights"/></p>';
            echo '</div>';
        }
        echo '<div><span>Add rights </span><input type="text" class="add_user_right_name"/>';
        echo '<SELECT class="add_user_right">
        <option>r</option>
        <option>w</option>
        <option>x</option>
        <option>rw</option>
        <option>rx</option>
        <option>wx</option>
        <option>rwx</option>        
        </SELECT>';
        echo '<button class="send_new_right">Add right</button></div>';
    }

    public function add_repo($name){
        shell_exec("python3 blih.py -u " . $this->auth->get_mail() . " -t \"" . $this->auth->get_password() . "\" repository create " . $name);
        shell_exec("python3 blih.py -u " . $this->auth->get_mail() . " -t \"" . $this->auth->get_password() . "\" repository setacl " . $name . " ramassage-tek rx");
        \Flight::redirect("/repos");
    }

    public function del_repo($name){
        if (empty($_SESSION['auth']) || $_SESSION['auth']->is_logged() != Auth::CONNECTED){
            return;
        }
        shell_exec("python3 blih.py -u " . $this->auth->get_mail() . " -t \"" . $this->auth->get_password() . "\" repository delete " . $name);
    }

    public function add_key($file){
        if (empty($_SESSION['auth']) || $_SESSION['auth']->is_logged() != Auth::CONNECTED){
            return;
        }
        shell_exec("python3 blih.py -u " . $this->auth->get_mail() . " -t \"" . $this->auth->get_password() . "\" sshkey upload " . $file);
    }

    public function del_key($name){
        if (empty($_SESSION['auth']) || $_SESSION['auth']->is_logged() != Auth::CONNECTED){
            return;
        }
        shell_exec("python3 blih.py -u " . $this->auth->get_mail() . " -t \"" . $this->auth->get_password() . "\" sshkey delete " . $name);
    }

    public function del_rights($repo, $user){
        shell_exec("python3 blih.py -u " . $this->auth->get_mail() . " -t \"" . $this->auth->get_password() . "\" repository " ."setacl ". $repo . " " . $user);
    }

    public function change_rights($repo, $user, $rights){
        //echo "python3 blih.py -u " . $this->auth->get_mail() . " -t \"" . $this->auth->get_password() . "\" repository " ."setacl ". $repo . " " . $user . " " . $rights;
        shell_exec("python3 blih.py -u " . $this->auth->get_mail() . " -t \"" . $this->auth->get_password() . "\" repository " ."setacl ". $repo . " " . $user . " " . $rights);
    }

}