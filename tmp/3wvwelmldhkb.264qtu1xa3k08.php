<h2 class="w3-text-light-grey">Repos page</h2>
<hr style="width:200px" class="w3-opacity">

<?php if ($repos): ?>
    

            <form action="/add_repo" method="post">
                <p>
                    <label for="name_repo">Add repo :</label>
                    <input type="text" name="name_repo" />
                    <input type="submit" value="Add" />
                </p>
            </form>
        <div class="w3-margin-bottom center-txt">
            <ul class="w3-ul w3-white w3-center w3-opacity ">
                <li class="w3-dark-grey w3-xlarge w3-padding-32"><?= $user->getName() ?> Repos</li>
                <?php foreach (($repos?:[]) as $repo): ?>
                    <li class="w3-padding-16"><?= $repo ?></li>
                <?php endforeach; ?>
            </ul>
        </div>

    
    <?php else: ?>

        <p>
            You are not logged
        </p>

    
<?php endif; ?>
