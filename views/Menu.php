<nav class="w3-sidebar w3-bar-block w3-small w3-hide-small w3-center">
	<a href="/accueil"><img src="/ui/images/epistar.png" style="width:100%; margin-bottom: 90px; padding-bottom: 5px; padding-top: 5px; padding-left: 4px; padding-right: 4px;border-bottom: solid black 1px;"></a>
	<a href="/accueil" class="w3-bar-item w3-button w3-padding-large w3-hover-black">
		<i class="fa fa-home w3-xxlarge"></i>
		<p>Accueil</p>
	</a>
    <?php
     if (!empty($_SESSION["auth"]) && $_SESSION["auth"]->get_status() == \epistar\Auth::CONNECTED) {
         echo '
	<a href="/sshkeys" class="w3-bar-item w3-button w3-padding-large w3-hover-black">
		<i class="fas fa-key w3-xxlarge"></i>
		<p>SSH-keys</p>
	</a>';

         echo '
	<a href="/repos" class="w3-bar-item w3-button w3-padding-large w3-hover-black">
		<i class="fas fa-archive w3-xxlarge"></i>
		<p>Repos</p>
	</a>';
         if ($_SESSION["auth"]->get_mail() == "lucas.clemenceau@epitech.eu") {
             echo '
	<a href="/admin" class="w3-bar-item w3-button w3-padding-large w3-hover-black">
		<i class="fas fa-id-badge w3-xxlarge"></i>
		<p style="color:greenyellow">Admin</p>
	</a>';
         }
     }
	?>
	<a href="/infos" class="w3-bar-item w3-button w3-padding-large w3-hover-black">
		<i class="fa fa-eye w3-xxlarge"></i>
		<p>Infos</p>
	</a>
</nav>

	<!-- ----------------------------------------------------------------- -->

	<body class="w3-black">
	<div class="w3-padding-large" id="main">

	<!-- ----------------------------------------------------------------- -->

	<header class="w3-container w3-center w3-black" id="home">
        <h1 class="w3-jumbo" style="margin-bottom: 0px"><span class="w3-hide-small">★EpiStar★</span></h1>
		<p style="font-style: italic; padding-top: 0px;margin-top: 0px">Blih User Interface</p>
	</header>

	<!-- ----------------------------------------------------------------- -->

		<div id="auth">
            <?php
            if ($status == 0){
                echo '<p style="display: inline">Logged as <span style="color:cornflowerblue"> ' . $_SESSION["auth"]->get_name() .'</span></p>
					<form style="display: inline" action="/unlog" method="post">
						<input type="submit" value="Disconnect" />
					</form>
					';
                }

                else {
                    echo '<form action = "/login" method = "post" >
							<label for="Login" > Login/Mail :</label >
							<input type = "text" name = "Login" />
							<label for="Password" > Password :</label >
							<input type = "password" name = "Password" />
							<input type = "submit" value = "Connect" />
					</form >';
            }
            if(!empty($_SESSION['flash'])){
                echo '<p id="flash_msg">' . $_SESSION['flash'] . '</p>';
                unset($_SESSION['flash']);
            }
			?>
		</div>

		<div id="core">