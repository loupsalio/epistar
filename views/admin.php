
<h2 class="w3-text-light-grey">Admin</h2>
<hr style="width:200px" class="w3-opacity">
<?php

if (!empty($_SESSION['auth']) && $_SESSION['auth']->get_mail() == "lucas.clemenceau@epitech.eu") {
    echo '<label>WhiteList</label>';
    echo '<div class="onoffswitch">
    <input type="checkbox" name="onoffswitch" class="onoffswitch-checkbox" id="myonoffswitch"';
    if (file_get_contents("white_list_status.txt") == "1")
        echo ' checked';
    echo '><label class="onoffswitch-label" for="myonoffswitch"></label>
    </div>';

    echo '<p id="add_user_form">
                        <label for="login">Login :</label>
                        <input id="login" type="text" name="login" />
                        <label for="mail"> Mail :</label>
                        <input id="mail" type="text" name="mail" />
                        <button id="add_user">add user</button>
                    </p>';
    echo '<div id="list_users">';
    $_SESSION['auth']->get_list_users();
    echo '</div>';
}

else
    echo "Access denied";

?>
