<h2 class="w3-text-light-grey">Accueil</h2>
<hr style="width:200px" class="w3-opacity">

<?php
    $tmp = $_SESSION["auth"];
    if ($tmp->is_logged() == \epistar\Auth::CONNECTED) {
        echo '<p>Bienvenue <span style="color:cornflowerblue;">' . $tmp->get_name() .'</span></p>
        <p>Mail : <span style="color:cornflowerblue ;">' . $tmp->get_mail() .'</span></p>
        <p>Pour toute erreur de mail, contactez-nous : master@epistar.under-wolf.eu</p>';
    }
?>
<p>Online EpiStar V 1.1a</p>
<p>Whitelist :
    <?php
        if(file_get_contents("white_list_status.txt") == "1"){
            echo '<span id="wl-on">ON</span>';
        }
        else
            echo '<span id="wl-off">OFF</span>';
    ?>
</p>
