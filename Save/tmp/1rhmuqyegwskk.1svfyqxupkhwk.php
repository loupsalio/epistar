  <!DOCTYPE html>
  <html lang="en">
  <head>
    <base href="" />
    <link rel="stylesheet" href="lib/code.css" type="text/css" />
    <link rel="stylesheet" href="ui/css/theme.css" type="text/css" />
    <script src="http://ajax.googleapis.com/ajax/libs/jquery/1/jquery.min.js"></script>
    <script src="ui/js/menu.js"></script>
  </head>
  <body>
    <div>
      <img class="header_img" src="ui/images/base.png">
      <h1 id="header_title">Name Site</h1>
    </div>
    <div id="main_block">
    <nav>
      <div id="menu_actual">
        <h2><?= $menu_list[0]['name_page'] ?></h2>
      </div>
      <ul id="navigation">
        <!-- <li class="menu_elem"><a title="aller à la section 1">Page 1</a></li> -->
        <?php foreach (($menu_list?:[]) as $item): ?>
          <li class="menu_elem" id="<?= $item['id'] ?>"><a><?= $item['name_page'] ?></a></li>
        <?php endforeach; ?>
      </ul>
    </nav>
  <div class="center" id="main_content">
    <p id="content_p">
      <?= $content.PHP_EOL ?>
      <!-- <?php echo $this->render($content,NULL,get_defined_vars(),0); ?> -->
    </p>
  </div>
  </div>
  </body>
  </html>
