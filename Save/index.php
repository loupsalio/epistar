  <?php

  // Kickstart the framework
  $f3 = require('lib/base.php');

  function page_list() {
        $db = new DB\SQL('mysql:host=localhost;dbname=main_base', 'root', 'starwolf');
      return $db->exec('SELECT * FROM list_page');
  }

  function page_content($id){
    $db = new DB\SQL('mysql:host=localhost;dbname=main_base', 'root', 'starwolf');
    return $db->exec('SELECT content FROM content_page WHERE id = ' . $id );
  }

  $f3->set('DEBUG', 1);
  if ((float) PCRE_VERSION < 7.9)
      trigger_error('PCRE version is out of date');


  // Load configuration
  $f3->config('config.ini');

  $f3->set('menu_list', page_list());

  $f3->map('notFound', function(){
        include 'ui/404.htm';
    });

  $f3->route('GET /', function($f3) {
    //  var_dump(page_content(1));
      $f3->set('content', page_content(1)[0]['content']);
      echo Template::instance()->render('layout.htm');
  }
  );

  $f3->route('GET /admin', function($f3) {
    if ($f3->get('SESSION.log') == 0)
    {
      echo Template::instance()->render('admin.htm');
    }
    else
    {
      $f3->set('link', str_replace("</br>", "\n", page_content(1)[0]['content']));
      echo Template::instance()->render('modif.htm');
    }
  }
  );

  $f3->route('POST /content_page', function($f3) {
      if (isset($_POST['id']))
        echo (page_content($_POST['id'])[0]['content']);
      else
        echo ("Error page");
  }
  );

  $f3->route('POST /log', function($f3) {
      $db = new DB\SQL('mysql:host=localhost;dbname=main_base', 'root', 'starwolf');
      $mapper = new DB\SQL\Mapper($db, 'users');
      $auth = new Auth($mapper, array('id' => 'username', 'pw' => 'password'));

      if ($auth->login($f3->get('POST.login'), $f3->get('POST.pass')) == true) {
          $f3->set('SESSION.log', 1);
      } else {
          $f3->set('SESSION.log', 0);
      }
      $f3->reroute('/admin');
  });

  $f3->route('POST /modif_content_db', function($f3) {
    $db = new DB\SQL('mysql:host=localhost;dbname=main_base', 'root', 'starwolf');
    if (isset($_POST['id']) && isset($_POST['content']))
      $db->exec("UPDATE content_page SET content = '" . str_replace("\n","</br>", $_POST['content'])  . "' WHERE id='" . $_POST['id'] . "'");
  });

  $f3->route('POST /unlog', function($f3) {
      $f3->set('SESSION.log', 0);
      $f3->reroute('/admin');
  });

  $f3->run();
