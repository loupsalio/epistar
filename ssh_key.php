<?php
/**
 * Created by PhpStorm.
 * User: Windows
 * Date: 25/02/2018
 * Time: 00:24
 */

namespace epistar {
    class ssh_key
    {
        const head = "ssh-rsa";

        private $name;
        private $core;

        public function __construct($name, $core)
        {
            $this->name = $name;
            $this->core = $core;
        }

        /**
         * @return mixed
         */
        public function getCore()
        {
            return $this->core;
        }

        /**
         * @return mixed
         */
        public function getName()
        {
            return $this->name;
        }
    }
}