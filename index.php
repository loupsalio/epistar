<?php

use epistar\page_engine;
use epistar\Auth;

require 'flight/Flight.php';
require 'page_engine.php';
require 'Auth.php';


// Database configuration

Flight::register('db', 'PDO', array('mysql:host=localhost;dbname=epistar','jack','starwolf'));

// Login initialize

session_start();
if (empty($_SESSION["auth"])){
    $auth = new Auth();
}
else
    $auth = $_SESSION["auth"];


// Layout templates initialize

Flight::render('Header', "", 'header_content');
Flight::render('Menu', array("status" => $auth->is_logged()), 'menu_content');
Flight::render('Footer', "", 'footer_content');

// Routes definitions

$root_engine = new page_engine();

if (!empty($_POST['name'])){
    Flight::set('repo_name', $_POST['name']);
    Flight::set('key_name', $_POST['name']);
}

if (!empty($_POST['login'])){
    Flight::set('login', $_POST['login']);
}

if (!empty($_POST['user'])){
    Flight::set('user', $_POST['user']);
}

if (!empty($_POST['name'])){
    Flight::set('name', $_POST['name']);
}

if (!empty($_POST['rights'])){
    Flight::set('rights', $_POST['rights']);
}

if (!empty($_POST['mail'])){
    Flight::set('mail', $_POST['mail']);
}

if (!empty($_POST['passwd'])){
    Flight::set('passwd', $_POST['passwd']);
}

if (!empty($_FILES['ssh_key'])){
    Flight::set('ssh_key_file', $_FILES['ssh_key']);
}

\Flight::set("page_engine", $root_engine);

Flight::route('GET /', array($root_engine, 'accueil'));
Flight::route('GET /accueil', array($root_engine, 'accueil'));
Flight::route('GET /repos', array($root_engine, 'repos'));
Flight::route('GET /infos', array($root_engine, 'infos'));
Flight::route('GET /admin', array($root_engine, 'admin'));
Flight::route('GET /sshkeys', array($root_engine, 'ssh_keys'));

Flight::route('POST /login', array($auth, 'login'));
Flight::route('POST /unlog', array($auth, 'unlog'));
Flight::route('POST /acl', array($root_engine, 'acl'));
Flight::route('POST /add', array($root_engine, 'add'));
Flight::route('POST /del', array($root_engine, 'del'));
Flight::route('POST /add_user', array($auth, 'add_user'));
Flight::route('POST /del_user', array($auth, 'del_user'));
Flight::route('POST /add_key', array($root_engine, 'add_key'));
Flight::route('POST /del_key', array($root_engine, 'del_key'));
Flight::route('POST /change_rights', array($root_engine, 'change_rights'));
Flight::route('POST /del_rights', array($root_engine, 'del_rights'));
Flight::route('POST /set_on', function (){
    file_put_contents("white_list_status.txt", "1");
});
Flight::route('POST /set_off', function (){
  file_put_contents("white_list_status.txt", "0");
});



Flight::map('notFound', function(){
    page_engine::full_render('404');
});

// Running Flight

Flight::start();

?>
