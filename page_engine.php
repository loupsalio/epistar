<?php
/**
 * Created by PhpStorm.
 * User: Near
 * Date: 06/02/2018
 * Time: 11:12
 */


namespace epistar {
    require 'Repo_engine.php';

    class page_engine
    {

        private $engine;

        public function __construct()
        {
            $this->engine = new \epistar\Repo_engine();
        }

        static public function full_render($file)
        {
                \Flight::render($file, "", 'body_content');
                \Flight::render('layout');
        }

        public function accueil()
        {
            $this->full_render('accueil');
        }

        public function admin(){
            if (empty($_SESSION['auth']) || $_SESSION['auth']->is_logged() != Auth::CONNECTED){
                self::full_render("not_log");
            }
            else {
                self::full_render("admin");
            }
        }

        public function repos()
        {
            if (empty($_SESSION['auth']) || $_SESSION['auth']->is_logged() != Auth::CONNECTED){
                self::full_render("not_log");
            }
            else {
                $this->full_render('repos');
            }
        }

        public function acl(){
            $this->engine->get_acl(\Flight::get('repo_name'));
        }

        public function add(){
            $this->engine->add_repo(\Flight::get('repo_name'));
        }

        public function del(){
            $this->engine->del_repo(\Flight::get('repo_name'));
            self::get_repos_list();
        }

        public function add_key(){
            $file = \Flight::get('ssh_key_file');

            $tmp_file = $file['tmp_name'];
            if( !is_uploaded_file($tmp_file) )
            {
                echo "Le fichier est introuvable";
                return;
            }
            if( preg_match('#[\x00-\x1F\x7F-\x9F/\\\\]#', $file['name']) )
            {
               echo "Nom de fichier non valide";
               return;
            }
            $this->engine->add_key($tmp_file);
            \Flight::redirect("/sshkeys");
        }

        public function del_key(){
            $this->engine->del_key(\Flight::get('key_name'));
            self::get_keys_list();
        }

        public function infos()
        {
            $this->full_render('infos');
        }

        public function ssh_keys()
        {
            if (empty($_SESSION['auth']) || $_SESSION['auth']->is_logged() != Auth::CONNECTED){
                self::full_render("not_log");
            }
            else {
                $this->full_render('sshkeys');
            }
        }

        public function change_rights(){
            $this->engine->change_rights(\Flight::get('repo_name'), \Flight::get('user'), \Flight::get('rights'));
            $this->acl();
         }

        public function del_rights(){
            $this->engine->del_rights(\Flight::get('repo_name'), \Flight::get('user'));
            $this->acl();
         }

        public function get_repos_list(){
            if (empty($_SESSION['auth']) || $_SESSION['auth']->is_logged() != Auth::CONNECTED){
                echo "You are not login, please reload";
                return;
            }
            else {
                $repos = $this->engine->get_repos_list();
                if ($repos == null){
                    echo "Password error. Try to reload.";
                    $_SESSION['auth']->unlog();
                    return;
                }
                foreach ($repos as $n)
                    echo '
                    <div class="repo-div">
                        <li class="w3-padding-16 name_of_repo">' . $n . '</li>
                        <div class="repo-options">
                            <p class="rights"></p>
                            <p class="delete-button">DELETE</p>
                        </div>
                    </div>';
            }
        }

        public function get_keys_list(){
                        if (empty($_SESSION['auth']) || $_SESSION['auth']->is_logged() != Auth::CONNECTED){
                echo "You are not login, please reload";
                return;
            }
            else {
                $keys = $this->engine->get_keys_list();
                if ($keys == null){
                    echo "Password error. Try to reload.";
                    $_SESSION['auth']->unlog();
                    return;
                }
                foreach ($keys as $n)
                    echo '
                    <div class="key-div">
                       <li class="w3-padding-16">' . $n->getName() . '</li>
                        <div class="key-options">
                            <p>'.$n->getCore().'</p>
                            <p class="key-delete-button">DELETE</p>
                    </div>
                    </div>';
            }
        }

    }

}