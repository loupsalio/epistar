
$(document).ready(function () {

    $('body').on("click", "#menu_actual", function () {
      $('.menu_elem').toggle(500);
    });
    $('body').on("click", ".menu_elem", function () {
      $('#menu_actual').children("h2").text($(this).text());
      $('.menu_elem').hide(500);
      var tmp =$(this).attr('id');
      $.ajax({
          method: 'POST',
          url: 'content_page',
          dataType: 'html',
          data: {id: tmp},
          success: function (result) {
              $("#content_p").html(result);
          },
          error: function (result) {
            console.log(result);
          }
      });
    });

    $('body').on("mouseover", "#menu_actual", function () {
      $(this).css('cursor', 'pointer')
    });
    $(this).on("mouseover", ".menu_elem", function () {
      $(this).css('cursor', 'pointer')
    });

    $('body').on("click", "#menu_actual_modif", function () {
      $('.menu_elem_modif').toggle(500);
    });
    $('body').on("click", ".menu_elem_modif", function () {
      $('#menu_actual_modif').children("h2").text($(this).text());
      $('.menu_elem_modif').hide(500);
      var tmp = $(this).attr('id');
      $('#modif_content').attr("elemtomodif", tmp);
      $.ajax({
          method: 'POST',
          url: 'content_page',
          dataType: 'html',
          data: {id: tmp},
          success: function (result) {
              $("#content_m").html(result);
          },
          error: function (result) {
            console.log(result);
          }
      });
    });
    $('body').on("mouseover", "#menu_actual_modif", function () {
      $(this).css('cursor', 'pointer')
    });
    $(this).on("mouseover", ".menu_elem_modif", function () {
      $(this).css('cursor', 'pointer')
    });

});
