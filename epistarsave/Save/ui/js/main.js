
  $(document).ready(function () {
      $('body').on("click", "#modif_content", function () {
          $.ajax({
              method: 'POST',
              url: 'modif_content_db',
              dataType: 'html',
              data: {id: $(this).attr('elemtomodif'), content: $('#content_m').val()},
              success: function (result) {
                  console.log(result);
                  $('#notif').css("background", "rgba(0, 255, 0, 0.4)");
                  $('#notif').text("Texte modifié");
              },
              error: function (result) {
                $('#notif').css("background", "rgba(255, 0, 0, 0.4)");
                $('#notif').text("Texte non modifié");
              }
          });
      });
  });
