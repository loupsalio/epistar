<script src="ui/js/main.js"></script>
<div class="content">
    <div id="block_disc">
        <form action="/unlog" method="post">
            <div class="button">
                <button type="submit">Disconnect</button>
            </div>
        </form>
    </div>
    <div id="current_list">
        <?php echo $this->render($actu_list,NULL,get_defined_vars(),0); ?>
    </div>
    <p id="h_line"></p>
    <div id="add_list">
        <div id="f_login">
            <div class="add_l">
                <SELECT id="h" name="h" size="1">
                    <OPTION>00
                    <OPTION>01
                    <OPTION>02
                    <OPTION>03
                    <OPTION>04
                    <OPTION>05
                    <OPTION>06
                    <OPTION>07
                    <OPTION>08
                    <OPTION>09
                    <OPTION>10
                    <OPTION>11
                    <OPTION>12
                    <OPTION>13
                    <OPTION selected>14
                    <OPTION>15
                    <OPTION>16
                    <OPTION>17
                    <OPTION>18
                    <OPTION>19
                    <OPTION>20
                    <OPTION>21
                    <OPTION>22
                    <OPTION>23
                </SELECT>
            </div>
            <p class="add_m">H</p>
            <div  class="add_l">
                <SELECT id="m" name="m" size="1">
                    <OPTION>00
                    <OPTION>01
                    <OPTION>02
                    <OPTION>03
                    <OPTION>04
                    <OPTION>05
                    <OPTION>06
                    <OPTION>07
                    <OPTION>08
                    <OPTION>09
                    <OPTION>10
                    <OPTION>11
                    <OPTION>12
                    <OPTION>13
                    <OPTION>14
                    <OPTION>15
                    <OPTION>16
                    <OPTION>17
                    <OPTION>18
                    <OPTION>19
                    <OPTION>20
                    <OPTION>21
                    <OPTION>22
                    <OPTION>23
                    <OPTION>24
                    <OPTION>25
                    <OPTION>26
                    <OPTION>27
                    <OPTION>28
                    <OPTION>29
                    <OPTION>30
                    <OPTION>31
                    <OPTION>33
                    <OPTION>33
                    <OPTION>34
                    <OPTION>35
                    <OPTION>36
                    <OPTION>37
                    <OPTION>38
                    <OPTION>39
                    <OPTION>40
                    <OPTION>41
                    <OPTION>44
                    <OPTION>44
                    <OPTION>44
                    <OPTION>45
                    <OPTION>46
                    <OPTION>47
                    <OPTION>48
                    <OPTION>49
                    <OPTION>50
                    <OPTION>51
                    <OPTION>55
                    <OPTION>53
                    <OPTION>54
                    <OPTION>55
                    <OPTION>56
                    <OPTION>57
                    <OPTION>58
                    <OPTION>59
                </SELECT>
            </div>
            <div class="add_l">
                <label for="infos">Description :</label>
                <input type="text" id="infos" name="infos" />
            </div>
            <div class="button add_l">
                <button type="submit" id="send_list">Ajouter</button>
            </div>
        </div>
    </div>
</div>
<div class="footer center">
    <p></p>
</div>
