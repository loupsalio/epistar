<!DOCTYPE html>
<html lang="en">
    <head>
        <base href="" />
        <link rel="stylesheet" href="lib/code.css" type="text/css" />
        <link rel="stylesheet" href="ui/css/base.css" type="text/css" />
        <link rel="stylesheet" href="ui/css/theme.css" type="text/css" />
        <script src="http://ajax.googleapis.com/ajax/libs/jquery/1/jquery.min.js"></script>
        <script src="ui/js/main.js"></script>
    </head>
    <body>
        <div>
            <img class="header_img" src="ui/images/base.png">
            <h1 id="header_title">Under-Wolf</h1>
            <p id="h_line"></p>

        </div>
    <?php echo $this->render($content,NULL,get_defined_vars(),0); ?>
</body>
</html>
