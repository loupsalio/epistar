  <?php foreach (($ulist?:[]) as $item): ?>
        <div>
            <?php if ($item['minutes'] >= 10): ?>
            <span id="<?= $item['id'] ?>"><?= $item['heure'] ?>H<?= $item['minutes'] ?> [<?= $item['infos'] ?>]</span>
            <?php endif; ?>
            <?php if ($item['minutes'] < 10): ?>
            <span id="<?= $item['id'] ?>"><?= $item['heure'] ?>H0<?= $item['minutes'] ?> [<?= $item['infos'] ?>]</span>
            <?php endif; ?>
            <img class="delete" src="ui/images/delete.png"/>
        </div>
    <?php endforeach; ?>