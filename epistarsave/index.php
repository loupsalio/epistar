<?php

// ----------- PAGES ----------- //

class page{

    private $f3;
    private $db;

    function __construct()
    {
        $this->f3 = $GLOBALS["f3"];
        $this->db = $GLOBALS["db"];
    }

    function set($field, $value){
        $this->f3->set($field, $value);
    }

    function get($field){
        return $this->f3->get($field);
    }

    public function get_pwd($name)
    {
        $res = $this->db->exec('SELECT password FROM users WHERE login = "' . $name . '"');
        return Chiffrement::decrypt($res[0]["password"]);
    }

    public function repos_list($mail, $password){
        $str = shell_exec("python3 blih.py -u " . $mail . " -t \"" . $password . "\" repository list");
        $res = explode("\n", $str);
        asort($res);
        array_shift($res);
        $this->f3->set('repos', $res);
    }

    public function accueil(){
        views::display("accueil");
	}

    public function infos(){
        views::display("infos");
    }

    public function core_admin(){
        views::display("admin_core");
    }

	public function repos(){
        $tmp_user = $this->f3->get('user');
        if(!is_null($tmp_user))
            $this->repos_list($tmp_user->getMail(), $this->get_pwd($tmp_user->getName()));
		views::display("repos");
	}

	public function add_repo(){
	$tmp_user = $this->f3->get('user');
        if (!is_null($_POST['name_repo']))
	   $this->blih_add($tmp_user->getMail(), $this->get_pwd($tmp_user->getName()), $_POST['name_repo']);
        $this->f3->reroute('/repos');
	}

	public function acl(){
        $tmp_user = $this->f3->get('user');
        $str = $tmp_user->getacl($_POST['name']);
        $last = str_replace('HTTP Error 404 Error message : ', "", $str);
        echo "Droits : " . $last;
    }

    public function delete(){
        $tmp_user = $this->f3->get('user');
        $str = $tmp_user->delete($_POST['name']);
    }

}

// ----------- VIEW ----------- //

class views{
	public static function display($name){
		echo \Template::instance()->render("views/Header.htm");
		echo \Template::instance()->render("views/Menu.htm");
		echo \Template::instance()->render("views/" . $name . ".htm");
		echo \Template::instance()->render("views/Footer.htm");
	}

	public static function display_short($name){
		echo \Template::instance()->render("views/" . $name . ".htm");
	}
}

// ----------- USER ----------- //

class user{
    public $id;
    public $name;
    private $mail;
    private $pwd;

    public function __construct($id_, $name_, $mail_, $pwd_)
    {
        $this->id = $id_;
        $this->name = $name_;
        $this->mail = $mail_;
        $this->pwd = $pwd_;
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @return mixed
     */
    public function getMail()
    {
        return $this->mail;
    }

    /**
     * @param mixed $mail
     */
    public function setMail($mail)
    {
        $this->mail = $mail;
    }

    public function getacl($name){
        $str = shell_exec("python3 blih.py -u " . $this->mail . " -t \"" . $this->pwd . "\" repository getacl " . $name);
        return $str;
    }

    public function delete($name){
        $str = shell_exec("python3 blih.py -u " . $this->mail . " -t \"" . $this->pwd . "\" repository delete " . $name);
        return $str;
    }
}

class Chiffrement
{

// Algorithme utilisé pour le cryptage des blocs
    private static $cipher = MCRYPT_RIJNDAEL_128;

// Clé de cryptage
    private static $key = "5a538d8d-9b54-4a78-8bdc-859c3ed28ff1";

// Mode opératoire (traitement des blocs)
    private static $mode = 'cbc';

    public static function crypt($data)
    {
        $keyHash = md5(self::$key);
        $key = substr($keyHash, 0, mcrypt_get_key_size(self::$cipher, self::$mode));
        $iv = substr($keyHash, 0, mcrypt_get_block_size(self::$cipher, self::$mode));

        $data = mcrypt_encrypt(self::$cipher, $key, $data, self::$mode, $iv);

        return base64_encode($data);
    }

    public static function decrypt($data)
    {
        $keyHash = md5(self::$key);
        $key = substr($keyHash, 0, mcrypt_get_key_size(self::$cipher, self::$mode));
        $iv = substr($keyHash, 0, mcrypt_get_block_size(self::$cipher, self::$mode));

        $data = base64_decode($data);
        $data = mcrypt_decrypt(self::$cipher, $key, $data, self::$mode, $iv);

        return rtrim($data);
    }
}

// ----------- CORE ----------- //

class core
{
    private $f3;
    private $db;
    private $auth;

    function __construct()
    {
        session_start();
        $this->f3 = require('lib/base.php');
        $GLOBALS["f3"] = $this->f3;
        $this->init_routes();
        $this->db = new DB\SQL('mysql:host=localhost;port=3306;dbname=epistar',
            'jack',
            'starwolf'
        );
        $GLOBALS["db"] = $this->db;
        $db_mapper = new \DB\SQL\Mapper($this->db, 'users');
        $this->auth = new \Auth($db_mapper, array('id' => 'login', 'pw' => 'password'));
        $S_user = $_SESSION["user"];
        if ($S_user) {
            $this->f3->set('user', $S_user);
        }
    }

    public function sub($login ,$pwd, $mail){
        $this->db->exec('INSERT INTO `users` (`id`, `login`, `password`) VALUES (NULL, \'' . $login .'\', \'' .  Chiffrement::crypt($pwd) . '\')');
        $this->db->exec('INSERT INTO `mails` (`id`, `login`, `mail`) VALUES (NULL, \'' . $login . '\', \'' . $mail .'\');');
    }

    public function get_id($name)
    {
        $res = $this->db->exec('SELECT id FROM users WHERE login = "' . $name . '"');
        return $res[0]["id"];
    }

    public function get_pwd($name)
    {
        $res = $this->db->exec('SELECT password FROM users WHERE login = "' . $name . '"');
        return Chiffrement::decrypt($res[0]["password"]);
       }

    public function get_mail($name)
    {
        $res = $this->db->exec('SELECT mail FROM mails WHERE login = "' . $name . '"');
        return $res[0]["mail"];
    }

    public function connect($name, $pwd)
    {
        echo $pwd + " | " + $this->get_pwd($name);
            if ($pwd == $this->get_pwd($name)){
            $tmp_user = new user($this->get_id($name), $name, $this->get_mail($name), $pwd);
            $this->f3->set('user', $tmp_user);
            $_SESSION["user"] = $tmp_user;
        } else {
            $this->f3->set('user', null);
            $_SESSION["user"] = null;
            $this->f3->set('rights', null);
        }
    }

    public function disconnect()
    {
        $this->user = null;
        $_SESSION["user"] = null;
    }

    public function init_routes()
    {
        $this->f3->route('GET /', 'page->accueil');
        $this->f3->route('GET /accueil', 'page->accueil');
        $this->f3->route('GET /repos', 'page->repos');
        $this->f3->route('POST /connect', function () {
            if (!is_null($_POST["Login"]) && !is_null($_POST["Password"]))
                $this->connect($_POST["Login"], $_POST["Password"]);
            $this->f3->reroute('/accueil');
        });
 	$this->f3->route('POST /disconnect', function () {
            $this->disconnect();
            $this->f3->reroute('/accueil');
        });

//        $this->f3->route('POST /add_repo', 'page->add_repo');
        $this->f3->route('GET /infos', 'page->infos');
        $this->f3->route('POST /acl [ajax]','page->acl');
      //  $this->f3->route('POST /delete[ajax]','page->delete');
        $this->f3->route('GET /core', 'page->core_admin');
        $this->f3->route('POST /sub', function(){
            if(!is_null($_POST['login']) && !is_null($_POST['pwd']) && !is_null($_POST['mail'])){
                $this->sub($_POST['login'], $_POST['pwd'], $_POST['mail']);
            }
        });

    }

    public function run()
    {
        $this->f3->run();
    }
}

// ----------- BASE ----------- //

$core = new core;
$core->run();
