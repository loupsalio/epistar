$(document).ready(function(){
    $(".repo-div").click(function(){
        $(this).siblings().children(".repo-options").hide(600);
        var tmp = $(this);
        console.log(tmp.children('li').text());
        $.ajax({
            type: 'POST',
            url      : "/acl",
            data     : {"name": tmp.children('li').text()},
            error    : function(request, error) { // Info Debuggage si erreur
                alert("Erreur : responseText: "+request.responseText);
            },
            success  : function(data) {
                tmp.children(".repo-options").children(".rights").html(data);
                tmp.children(".repo-options").toggle(500);
            }
        });
    });
});
