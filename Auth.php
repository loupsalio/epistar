<?php
/**
 * Created by PhpStorm.
 * User: Windows
 * Date: 17/02/2018
 * Time: 16:47
 */

namespace epistar {

    class Auth
    {
        const CONNECTED = 0;
        const NOT_CONNECTED = 1;

        private $key;
        private $status;
        private $name;
        private $mail;
        private $password;

        public function __construct()
        {
         $_SESSION["auth"] = $this;
         $this->status = $this::NOT_CONNECTED;
        }

        public function encrypt($elem)
        {
            $key = file_get_contents("Keygen.txt");
            $data = serialize($elem);
            $td = mcrypt_module_open(MCRYPT_DES,"",MCRYPT_MODE_ECB,"");
            $iv = mcrypt_create_iv(mcrypt_enc_get_iv_size($td), MCRYPT_RAND);
            mcrypt_generic_init($td,$key,$iv);
            $data = base64_encode(mcrypt_generic($td, '!'.$data));
            mcrypt_generic_deinit($td);
            return $data;
        }

        public function decrypt($elem)
        {
            $key = file_get_contents("Keygen.txt");
            $td = mcrypt_module_open(MCRYPT_DES,"",MCRYPT_MODE_ECB,"");
            $iv = mcrypt_create_iv(mcrypt_enc_get_iv_size($td), MCRYPT_RAND);
            mcrypt_generic_init($td,$key,$iv);
            $data = mdecrypt_generic($td, base64_decode($elem));
            mcrypt_generic_deinit($td);

            if (substr($data,0,1) != '!')
                return false;

            $data = substr($data,1,strlen($data)-1);
            return unserialize($data);
        }

        public function check_login($name, $passwd)
        {
            $db = \Flight::db();
            $data = $db->query("SELECT * FROM users WHERE `name` =  '$name' OR `mail` =  '$name'");
            $result = $data->fetch(\PDO::FETCH_ASSOC);
            if (file_get_contents("white_list_status.txt") == "1" && $result == false) {
                $_SESSION['flash'] = "You are not in the whitelist";
                return false;
            }
            else if (file_get_contents("white_list_status.txt") == "0" && $result == false){
                $str = shell_exec("python3 blih.py -u " . $name . " -t \"" . self::decrypt($passwd) . "\" whoami");
                $t = strpos($str, "Error 401");
                if($t != false) {
                    return false;
                }
                $this->name = $name;
                $this->mail = $name;
                $this->password = $passwd;
                return true;
            }
            $str = shell_exec("python3 blih.py -u " . $result['mail'] . " -t \"" . self::decrypt($passwd) . "\" whoami");
            $t = strpos($str, "Error 401");
            if($t != false) {
                $_SESSION['flash'] = "Login/password error";
                return false;
            }
            $this->name = $name;
            $this->mail = $result['mail'];
            $this->password = $passwd;
            return true;
        }

        public function login()
        {
            if (empty($_POST['Login']) || empty($_POST['Password'])) {
                $this->status = $this::NOT_CONNECTED;
                $_SESSION['flash'] = "Login/password can't be empty";
                \Flight::redirect('accueil');
                return;
            } else {
                $name = $_POST['Login'];
                $password = $_POST['Password'];
            }
            if ($this->check_login($name, $this->encrypt($password))) {
                $this->status = $this::CONNECTED;
            } else {
                $this->status = $this::NOT_CONNECTED;
            }
            \Flight::redirect('accueil');
        }

        public function unlog()
        {
            $this->status = $this::NOT_CONNECTED;
            $_SESSION['auth'] = null;
            \Flight::redirect('accueil');
        }

        public function get_list_users()
        {
            $db = \Flight::db();
            $data = $db->query("SELECT * FROM users");
            echo '<TABLE BORDER="0">';
            while ($result = $data->fetch(\PDO::FETCH_ASSOC)) {
                echo '
  <TR>
    <TD align="left" WIDTH="300" class="user_login">' . $result['name'] . '</TD>
    <TD>&nbsp;</TD>
    <TD align="center" WIDTH="300">' . $result['mail'] . '</TD>
        <TD>&nbsp;</TD>';
if ($result['mail'] != "lucas.clemenceau@epitech.eu")
    echo '<TD align="right" WIDTH="40"><img class="delete_button_user" src="http://bioserver-3.bioacademy.gr/Bioserver/SBML-Viewer/images/red-delete-button.png"/></TD>';
    echo '</TR>';
            }
            echo '</TABLE>';
        }

        public function add_user(){
            $db = \Flight::db();
            $data = $db->query("SELECT * FROM users WHERE `name` =  '". \Flight::get('login') ."'");
            $res = $data->fetch(\PDO::FETCH_ASSOC);
            if ($res != false) {
                echo "<p style='color: red'>user already exist</p>";
                $this->get_list_users();
                return false;
            }
            echo "<p style='color: green'>user added</p>";
            $db->query("INSERT INTO `users` (`Id`, `name`, `mail`, `password`) VALUES (NULL, '".  \Flight::get('login')  ."', '".  \Flight::get('mail')  ."', '".  self::encrypt('----')  ."');");
            $this->get_list_users();
        }

        public function del_user(){
            $db = \Flight::db();
           $db->query("DELETE FROM `users` WHERE `name` = '" .\Flight::get('login') . "';");
        //    file_put_contents("./save.txt", "User " . \Flight::get('login') . " deleted\n",  FILE_APPEND);
            $this->get_list_users();
        }

        public function is_logged()
        {
            return $this->status;
        }

        public function get_name(){
            return $this->name;
        }

        public function get_mail(){
            return $this->mail;
        }

        public function get_password(){
            return self::decrypt($this->password);
        }

        public function get_status(){
            return $this->status;
        }

    }

}